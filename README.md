# Palindrom

1. Készítsünk programot, mely eldönti egy argumentumként megadott szövegről, hogy palindrom vagy sem!

    Palindromnak nevezünk egy szót vagy kifejezést, ha visszafele olvasva ugyanazt kapjuk.

    Feltesszük, hogy csak egy szót kapunk argumentumként.

    Például:

    $ java Palindrome noon
    true

    $ java Palindrome radar
    true

    $ java Palindrome tree
    false
    
2. Egészítsük ki úgy a programot, hogy az ne csak szavakat, hanem több szóból álló kifejezéseket is elfogadjon.

    Például:

    $ java Palindrome "race car"
    true

3. Írjunk egy programot, mely kicseréli egy szöveg minden kisbetűjét nagybetűre, minden nagybetűt kisbetűre, minden számjegyéhez hozzáad egyet és minden más karaktert aláhúzásjelre cserél!

    Az egyes karakterek besorolásához használjuk a java.lang.Character osztály statikus metódusait.

4. Készítsünk egy olyan függvényt, mely egy paraméterül kapott String szavainak sorrendjét megfordítja, de a szavakat változatlanul hagyja!

    Például:

    $ java ReverseWords "Alma fa alatt"
    alatt fa Alma
