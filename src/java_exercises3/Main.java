package java_exercises3;

import java.util.Scanner;
import java.lang.Character;

public class Main {
	public static void main(String[] args) {
		String word = args[0];
		String word2 = "";
		
		word = word.replaceAll(" ", "");
		for (int i = word.length()-1; i >= 0; i--) {
			word2 +=word.charAt(i);
		}
		
		System.out.println(word.equals(word2));
		
		
		String word3 = "";
		for (int i = 0; i < word.length(); i++) {
			if(Character.isLowerCase(word.charAt(i))) {
				word3 += Character.toUpperCase(word.charAt(i));
			}else if(Character.isUpperCase(word.charAt(i))) {
				word3 += Character.toLowerCase(word.charAt(i));
			}else if(Character.isDigit(word.charAt(i))) {
				String number = "" + word.charAt(i);
				int help = Integer.parseInt(number);
				help++;
				word3 += help;
			}else{
				word3 += "_";
			}	
		}
		System.out.println(word3);
		
		String example = "I like turtles";
		System.out.println(reversedSentecne(example));
		
	}
	public static String reversedSentecne(String words) {
		String reversed = "";
		String[] spaced = words.split(" ");
		for (int i = spaced.length-1; i >= 0 ; i--) {
			reversed += spaced[i];
			if (i != 0) {
				reversed += " ";
			}
			
		}
		
		return reversed;
	}
}
